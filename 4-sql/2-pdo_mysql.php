<?php

// buat object pdo
// untuk koneksi ke database
$dsn = "mysql:host=localhost;dbname=buku";

$db_user = "root";
$db_password = "";

$pdo = new PDO($dsn, $db_user, $db_password);

// ---

// ambil data dari tabel buku
$data = $pdo->query('SELECT * FROM buku');

// lakukan pengulangan untuk setiap baris data
foreach ($data as $baris_data)
{
    // print array
    print_r($baris_data);
    print("<br>");
    print("<br>");

    // print nilai dari tiap kolom
    print($baris_data['id']);
    print("<br>");
    print($baris_data['judul']);
    print("<br>");
    print($baris_data['pengarang']);
    print("<br>");
    print($baris_data['tahun']);
    print("<br>");

    print("<hr>");
}