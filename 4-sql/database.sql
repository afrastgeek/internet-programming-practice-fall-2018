-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.9-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for buku
DROP DATABASE IF EXISTS `buku`;
CREATE DATABASE IF NOT EXISTS `buku` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `buku`;

-- Dumping structure for table buku.buku
DROP TABLE IF EXISTS `buku`;
CREATE TABLE IF NOT EXISTS `buku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) DEFAULT '0',
  `pengarang` varchar(50) DEFAULT '0',
  `tahun` year(4) DEFAULT 2000,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table buku.buku: ~4 rows (approximately)
/*!40000 ALTER TABLE `buku` DISABLE KEYS */;
REPLACE INTO `buku` (`id`, `judul`, `pengarang`, `tahun`) VALUES
    (1, 'Judul Buku 1', 'Pengarang 1', '2000'),
    (2, 'Judul Buku 2', 'Pengarang 2', '2003'),
    (3, 'Judul Buku 3', 'Pengarang 3', '2007'),
    (4, 'Judul Buku 4', 'Pengarang 4', '2010');
/*!40000 ALTER TABLE `buku` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
