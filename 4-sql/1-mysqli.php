<?php

// buat object mysqli
// untuk koneksi ke database
$mysqli = new mysqli(
    "localhost", // host
    "root", // username
    "", // password
    "buku" // database name
);

// ---

// ambil data dari tabel buku
$hasil = $mysqli->query("SELECT * FROM buku");

// lihat hasil query
print_r($hasil);

// lakukan pengulangan untuk setiap baris data
while ($baris_data = $hasil->fetch_assoc()) {
    // print array
    print_r($baris_data);
    print("<br>");
    print("<br>");

    // print nilai dari tiap kolom
    print($baris_data['id']);
    print("<br>");
    print($baris_data['judul']);
    print("<br>");
    print($baris_data['pengarang']);
    print("<br>");
    print($baris_data['tahun']);
    print("<br>");

    print("<hr>");
}

?>