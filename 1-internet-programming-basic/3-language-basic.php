<?php

$variable_angka = 123;
$variable_float = 123.5;
$variable_string = "string";

// echo $variable_angka == 123;
// echo $variable_angka === 123;
// echo $variable_angka === "123";

if ($variable_string == "string") {
	echo '$variable_string berisi tulisan string';
	echo "<br>";
	echo "$variable_string berisi tulisan string";
	echo "<br>";
	echo $variable_string . " berisi tulisan string";
	echo "<br>";
	echo $variable_string . ' berisi tulisan string';
}

$variable_array2 = array(1, 2, 3);

for ($i=0; $i < 3; $i++) {
	echo "<br>";
	echo $i;
}

echo "<br>";

$variable_array1 = ['a', 'b', 'c'];
foreach ($variable_array1 as $value) {
	echo "<br>";
	echo $value;
}

echo "<br>";

$variable_array3 = array('mobil' => 10, 'motor' => 2);
foreach ($variable_array3 as $key => $value) {
	echo "<br>";
	echo "$key => $value";
}