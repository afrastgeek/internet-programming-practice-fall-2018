<?php

/**
 * Interface for makhluk hidup.
 */
interface MakhlukHidupInterface
{
	public function berjalan();
}

/**
 * Class for manusia.
 *
 * Ini file ke-5.
 * Tentang class php yang mendeskripsikan manusia
 */
class Manusia implements MakhlukHidupInterface
{
    // property declaration

    public $warna_kulit = 'coklat';
    protected $dna = 'dna';
    private $rahasia = 'secret';


    // method declaration

    public function __construct($warna = 'coklat')
    {
        $this->warna_kulit = $warna;
    }

    /**
     * Manusia bisa berjalan.
     */
    public function berjalan() {
        echo "manusia ini sedang berjalan";
    }
}

$ammar = new Manusia;
// print_r($ammar);

$firman = new Manusia('langsat');
print_r($firman);

$mahasiswa = array($ammar, $firman);
// var_dump($mahasiswa);