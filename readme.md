# Internet Programming Practice
Fall 2018
Universitas Pendidikan Indonesia

## Syllabus

> Terms:
> - Basics
> - Object Oriented Design [OO]
> - Application Architecture [Architecture]
> - Security
> - Framework

- Week 1
  - [x] [Basics] Syntax
  - [x] [OO] Programming to an interface
  - [ ] [OO] Favoring composition over inheritance
- Week 2
  - [x] [Basics] Request & Response
- Week 3
- Week 4
  - [x] [Basics] SQL Queries
  - [x] [Basics] Sessions & Cookies
- Week 5
  - [ ] [Basics] SQL Queries
  - [ ] [Architecture] MVC pattern
  - [ ] [Frameworks] Code Igniter (Configuration)
  - [ ] [Frameworks] Code Igniter (VC)
- Week 6
  - [ ] [Frameworks] Code Igniter (MVC)
  - [ ] [OO] S.O.L.I.D. principles
  - [ ] [Architecture] Basic understanding of Domain Driven Design
- Week 7
  - [ ] [Frameworks] Laravel (VC + Route)
  - [ ] [Architecture] Autoloading & using composer to manage external libraries and dependencies
  - [ ] [Architecture] Various design patterns
- Week 8
  - [ ] [Frameworks] Laravel (MVC)
  - [ ] [OO] Dependency injection
  - [ ] [Architecture] Using an Inversion of Control container for managing dependency injection
  - [ ] [OO] Namespaces
- Week 8
  - [ ] [Frameworks] Laravel (Testing)
  - [ ] [Architecture] Unit testing
  - [ ] [OO] Exceptions
- Week 9
  - [ ] [Frameworks] Laravel + VueJS
  - [ ] [Frameworks] Laravel (API)
- Week 10
  - [ ] [Frameworks] Laravel + VueJS SPA
- Week 11
  - [ ] [Frameworks] Laravel + VueJS SPA
- Optional
  - [ ] [Security] Cross Site Request Forgery protection (CSRF)
  - [ ] [Security] Filtering input to prevent malicious code execution
  - [ ] [Security] Filtering input and using PDO prepared statements to stop SQL injection
  - [ ] [Security] Escaping output to mitigate Cross Site Scripting vulnerabilities (XSS)
  - [ ] [Security] How to hash and store passwords (php 5.5 made this a lot easier)
  - [ ] [Security] How to safely upload images & files
  - [ ] [Security] Configuring your environment for maximum error checking
